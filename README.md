SUBMISSION
==========

This is the Team Entropy's contribution to the Tungsten Hackathon.

Please refer to the (internal) wiki page for further information
    https://github.com/dfinity-lab/dapps/wiki/Neurons

(Please note that this need not be a permanent repository,
and it may be removed any time.)

## Team *Entropy* members
- Mamiko Inoue
- Gabor Greif

## Purpose and Outcome

The purpose of our effort is to explore three avenues of research
- is the combinatorially flavoured neural evolution a possibility
  for distributed systems
- can _query methods_ and client-supplied randomness reduce the
  energy consumption
- explore feasibility of running Haskell code on the Internet Computer

The outcome in form of our submission doesn't reach the point of delivering a
runnable canister for the IC. However we have gained a bit if a glimpse of the
feasibility of externally-driven neural evolution. We didn't implement neuron
splitting, and the economic (reward) considerations are still out of view. We
don't track the forms of energy (free and potential) in our model yet.

We believe that running (hypotheses of) transformations as queries and when
determined as promising re-running as updates can reduce overall energy
consumption and latency.

The size of our contribution is small and appears unpolished. But it can be
compiled by Asterius [1] to a Wasm binary. (The latter is not runnable on the
replica yet, because it assumes a browser-like environment.)
We had a fallback strategy of rewriting the algorithm in Motoko after getting it
working, but that had to be abandoned as not feasible. These type of algorithms
really shine in Haskell.

[1] https://asterius.netlify.app

Gabor's personal thoughts follow...
-----------

The big idea was to keep some sort of personal vault in the IC, which can be fed
with snapshots of personal gadgets (e.g. by taking a photo) and use some
self-learning AI that would make sense of those artifacts. Ideally the data would
become searchable and accessible in different ways.

For that one would need some kind of image recognition. The approach we
experimented with is described in the linked wiki page.

I have fought a lot to have the Haskell code running on the IC. At the ZuriHac
weekend I have made myself knowledgeable with the Asterius Haskell compiler,
had conversations with its principal developer, and came to the conclusion that
it could be a nice contender for a next  IC language and runtime.

Unfortunately the toolchain needs much more work than I could afford  in the
Tungsten Hackathon. At least I could get Asterius to compile simple Haskell
programs to Wasm (on Linux, on the Mac it still has its share of quirks), and I
could inspect the output. It has a garbage collector and several features that
we don't even need. I believe it could be brought to stage where it would
interface nicely with the IC and become a productivity tool.

As a last push I'll try to compile this code with Asterius, and see if that works.

Indeed, it builds:
```
ggreif@vm-ggreif:~/asterius/hs-diatropy$ stack exec -- ahc-cabal new-install
Renaming /tmp/ahc-cabal-30234/config to /tmp/ahc-cabal-30234/config.backup.
Writing merged config to /tmp/ahc-cabal-30234/config.
Wrote tarball sdist to
/home/ggreif/asterius/hs-diatropy/dist-newstyle/sdist/hs-diatropy-0.1.0.0.tar.gz
Resolving dependencies...
Build profile: -w ghc-8.8.3 -O1
In order, the following will be built (use -v for more details):
 - hs-diatropy-0.1.0.0 (exe:hs-diatropy) (requires build)
Starting     hs-diatropy-0.1.0.0 (exe:hs-diatropy)
Building     hs-diatropy-0.1.0.0 (exe:hs-diatropy)
Installing   hs-diatropy-0.1.0.0 (exe:hs-diatropy)
Completed    hs-diatropy-0.1.0.0 (exe:hs-diatropy)
Symlinking 'hs-diatropy'
ggreif@vm-ggreif:~/asterius/hs-diatropy$ ls -l ~/.cabal/bin/hs-diatropy 
lrwxrwxrwx 1 ggreif ggreif 119 Jun 16 22:01 /home/ggreif/.cabal/bin/hs-diatropy -> ../store/ghc-8.8.3/hs-diatropy-0.1.0.0-f5f4b46f48e7dfe4514f6d073c1d4e5552da201289154b017664fdfd3d97dbd2/bin/hs-diatropy
ggreif@vm-ggreif:~/asterius/hs-diatropy$ ls -l /home/ggreif/.cabal/store/ghc-8.8.3/hs-diatropy-0.1.0.0-f5f4b46f48e7dfe4514f6d073c1d4e5552da201289154b017664fdfd3d97dbd2/bin/hs-diatropy
-rwxr-xr-x 1 ggreif ggreif 5513966 Jun 16 22:01 /home/ggreif/.cabal/store/ghc-8.8.3/hs-diatropy-0.1.0.0-f5f4b46f48e7dfe4514f6d073c1d4e5552da201289154b017664fdfd3d97dbd2/bin/hs-diatropy
```
It could be a bit smaller though...

I don't consider this hackathon a waste of time, we had a few highlights and our
fair share of frustrations. Next time we should start out with a more reliable
toolchain to not lose >50% of our time fighting the beasts.