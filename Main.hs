{-# language DeriveFunctor, DerivingStrategies, GeneralizedNewtypeDeriving, ViewPatterns #-}

module Main where

-- See https://github.com/dfinity-lab/dapps/wiki/Neurons

import Data.Set
import Data.Coerce
import Control.Monad
import Control.Comonad
import Control.Arrow
import Data.List (foldl')
import Debug.Trace (traceShowId)

allSame ps = Data.List.foldl' (==) (head ps) ps

type Loc = (Int, Int)

type Pixels = Set Loc

newtype PixelFrames a = Frames ([Pixels], Loc, a) -- should be Set Pixels
    deriving stock (Show, Functor)

instance Comonad PixelFrames where
    extract (Frames (_, _, a)) = a
    duplicate f@(Frames (map, loc, a)) = Frames (map, loc, f)


pixel :: PixelFrames a -> PixelFrames [Bool]
pixel (Frames (map, loc, _)) = Frames (map, loc, member loc <$> map)

-- absolute  movement (jump)
move :: PixelFrames a -> Loc -> PixelFrames a
move (Frames (map, _, a)) loc = (Frames (map, loc, a))

-- relative  movements (skip)
moveX, moveY :: PixelFrames a -> Int -> PixelFrames a
moveX f@(Frames (_, (x, y), _)) dx = move f (x + dx, y)
moveY f@(Frames (_, (x, y), _)) dy = move f (x, y + dy)
moveXY :: PixelFrames a -> Int -> Int -> PixelFrames a
moveXY f@(Frames (_, (x, y), _)) dx dy = move f (x + dx, y + dy)

range :: PixelFrames a -> [Int]
range (Frames (map, _, _)) = [0 .. length map - 1]

construct :: [[[Bool]]] -> PixelFrames ()
construct pxs
    = Frames ( (\px -> fromList [ (x, y) | (y, xs) <- zip [0..] px, (x, True) <- zip [0..] xs ]) <$> pxs
             , (0, 0)
             , ())


{-

* Aggregates
For now we completely avoid frustrations:
- we can't add new frames
- we cannot address and join neurons other than the front frame

We'll have to lift this restriction soon!
-}

-- A Neuron is an common occupied area in some frames

newtype Neuron = Neuron (Pixels, [Int])
    deriving newtype (Eq, Ord, Show)

neuronLoc :: Neuron -> Loc
neuronLoc (Neuron (ps, _)) = head $ toList ps

neuronFrames :: Neuron -> [Int]
neuronFrames (Neuron (_, fs)) = fs

neuronArea, neuronCircumference :: Num a => Neuron -> a
neuronArea (Neuron (ps, _)) = fromIntegral $ size ps

-- Cave: very naïve
neuronCircumference n@(Neuron (s, _)) = Data.Set.foldl proximity (neuronArea n * 4) (s `cartesianProduct` s)
    where proximity c ((x1, y1), (x2, y2)) =
              if (abs (x2 - x1) + abs (y2 - y1) == 1) then c - 1 else c

-- an Aggregate is a set of neurons populating (covering) a PixelFrames
-- some gaps can arise, namely where it would lead to frustration

data Aggregate a = Aggregate (Set Neuron) (PixelFrames a)
    deriving (Show, Functor)

instance Comonad Aggregate where
    extract (Aggregate _ pxs) = extract pxs
    duplicate agg@(Aggregate nd pxs) = Aggregate nd (const agg <$> pxs)

unfrustrated :: Int -> Int -> PixelFrames a -> Aggregate a
unfrustrated xmax ymax pxs = Aggregate ns pxs
    where ns = fromList [ Neuron (singleton loc, range pxs)
                        | x <- [0 .. xmax], y <- [0 .. ymax]
                        , let loc = (x, y)
                              pix = pixel $ move pxs loc
                        , allSame $ extract pix
                        ]
               `union`
                fromList [ Neuron (singleton loc, pure d)
                         | x <- [0 .. xmax], y <- [0 .. ymax]
                         , let loc = (x, y)
                               pix = pixel $ move pxs loc
                         , not $ allSame $ extract pix
                         , d <- range pix
                         ]



checkCovering, checkNoFrustrated :: Int -> Int -> Aggregate a -> Bool
checkCovering xmax ymax (Aggregate ns (range -> f)) = length all == length ours && fromList all == fromList ours
    where all = [ (x, y, z) | x <- [0 .. xmax], y <- [0 .. ymax], z <- f ]
          ours = [ (x, y, z) | Neuron (ls, ds) <- toList ns, (x, y) <- toList ls, z <- ds ]

checkNoFrustrated  xmax ymax (Aggregate ns pxs) = and good
    where good = [ allSame pix
                 | Neuron (ls, ds) <- toList ns
                 , loc <- toList ls
                 , let pix = [ extract at !! y | y <- ds, let at = pixel (move pxs loc) ]
                 ]

-- try joining neurons (all frames) and report whether something changed
--
joinNeurons :: Loc -> Loc -> Aggregate a -> Aggregate (Maybe Neuron)
joinNeurons a b (Aggregate ns pxs) = case j of
                                       Nothing -> Aggregate ns (const Nothing <$> pxs)
                                       Just (n, ns) -> Aggregate ((insert n ns)) (const (Just n) <$> pxs)
    where j = do [Neuron (an, ad), Neuron (bn, bd)] <- toList <$> pure abn
                 -- invariant: both don't contain frustrated pixels
                 guard $ ad == bd
                 pure (Neuron (coerce union an bn, ad), rest)

          (abn, rest) = partition (\(Neuron (n, _)) -> a `member` n || b `member` n) ns

-- try joining neurons (anchored at a specific frame) and report whether something changed
--
joinNeurons' :: Loc -> Loc -> Int -> Aggregate a -> Aggregate (Maybe Neuron)
joinNeurons' a b f (Aggregate ns pxs) = case j of
                                          Nothing -> Aggregate ns (const Nothing <$> pxs)
                                          Just (n, ns) -> Aggregate ((insert n ns)) (const (Just n) <$> pxs)
    where j = do [Neuron (an, ad), Neuron (bn, bd)] <- toList <$> pure abn
                 -- invariant: both don't contain frustrated pixels
                 guard $ ad == bd
                 pure (Neuron (coerce union an bn, ad), rest)

          (abn, rest) = partition (\(Neuron (n, fs)) -> f `elem` fs && (a `member` n || b `member` n)) ns


neuron :: Aggregate a -> Loc -> Maybe Neuron -- Cave: Global only, Nothing means some frustration
neuron agg@(Aggregate ns _) l =
    do [n] <- pure . toList $ Data.Set.filter (\(Neuron (n, _)) -> l `member` n) ns
       pure n

-- pick a neuron anchored at a frame
--
neuron' :: Aggregate a -> Loc -> Int -> Maybe Neuron
neuron' agg@(Aggregate ns _) l f =
    do [n] <- pure . toList $ Data.Set.filter (\(Neuron (n, fs)) -> f `elem` fs && l `member` n) ns
       pure n

neurons :: Aggregate a -> Set Neuron
neurons (Aggregate ns _) = ns


main :: IO ()
main = do print frames
          print (pixel frames)
          print (pixel $ moveXY frames 1 1)
          let us = unfrustrated 2 2 frames
          print us
          print $ joinNeurons (0, 0) (1, 0) us
          print $ fmap neuronArea $ extract $ joinNeurons (0, 0) (1, 1) us
          print $ fmap neuronCircumference $ extract $ joinNeurons (0, 0) (1, 0) us
          print $ extract $ joinNeurons (0, 0) (0, 1) us
          print $ neurons $ joinNeurons (0, 0) (0, 1) us
          let us1 = joinNeurons (0, 0) (1, 0) us
          let us2 = joinNeurons (0, 0) (2, 0) us1
          let us3 = joinNeurons (0, 0) (2, 1) us2
          let us4 = joinNeurons (0, 0) (2, 2) us3
          print $ fmap (neuronArea &&& neuronCircumference) $ extract $ us4
          let us5 = joinNeurons' (0, 2) (1, 2) 0 us4
          print $ extract us5
          let us6 = joinNeurons' (0, 1) (0, 2) 1 us5
          let us7 = joinNeurons' (0, 2) (1, 2) 1 us6
          print $ extract us7
          print $ checkCovering 2 2 <$> [const Nothing <$> us, us1, us2, us3, us4, us5, us6, us7]
          print $ checkNoFrustrated 2 2 <$> [const Nothing <$> us, us1, us2, us3, us4, us5, us6, us7]
          let us8 = joinNeurons' (1, 2) (1, 1) 1 us7 -- grab the middle clear pixel
          print $ extract $ us8
          print $ neuron' us8 (1, 1) 1

frames = construct $ (fmap . fmap) (/= ' ') <$> [f0, f1]
    where f0 = [ "###"
               , "# #"   -- intention: a /zero/ '0'
               , "###"
               ]
          f1 = [ "###"
               , "  #"   -- intention: a /seven/ '7'
               , "  #"
               ]
          f2 = [ "###"
               , "  #"   -- intention: a /three/ '3'
               , "###"
               ]
          f3 = [ "## "
               , " # "   -- intention: a /two/ '2'
               , "###"
               ]
          f4 = [ " # "
               , " # "   -- intention: a /one/ '1'
               , " # "
               ]
          f5 = [ "## "
               , "#  "   -- intention: a /five/ '5'
               , "###"
               ]
          f6 = [ "  #"
               , "## "   -- intention: a /six/ '6'
               , "## "
               ]
          f7 = [ " ##"
               , " ##"   -- intention: a /nine/ '9'
               , " # "
               ]
          f8 = [ " ##"
               , "###"   -- intention: a /four/ '4'
               , " # "
               ]
          f9 = [ "###"
               , " # "   -- intention: an /eight/ '8'
               , "###"
               ]

-- The following model 7-segment displays
grames = construct $ (fmap . fmap) (/= ' ') <$> [g0, g1]
    where g0 = [ " ####### "
               , "# ##### #"
               , "##     ##"
               , "##     ##"
               , "#       #"
               , "         "   -- intention: a /zero/ '0'
               , "#       #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "
               ]

          g1 = [ "        "
               , "        #"
               , "       ##"
               , "       ##"
               , "        #"
               , "         "   -- intention: a /one/ '1'
               , "        #"
               , "       ##"
               , "       ##"
               , "        #"
               , "         "
               ]

          g2 = [ " ####### "
               , "  ##### #"
               , "       ##"
               , "       ##"
               , "  ##### #"
               , " ####### "   -- intention: a /two/ '2'
               , "#        "
               , "##       "
               , "##       "
               , "# #####  "
               , " ####### "
               ]

          g3 = [ " ####### "
               , "  ##### #"
               , "       ##"
               , "       ##"
               , "  ##### #"
               , " ####### "   -- intention: a /three/ '3'
               , "        #"
               , "       ##"
               , "       ##"
               , "  ##### #"
               , " ####### "
               ]

          g4 = [ "         "
               , "#       #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "   -- intention: a /four/ '4'
               , "        #"
               , "       ##"
               , "       ##"
               , "        #"
               , "         "
               ]

          g5 = [ " ####### "
               , "# #####  "
               , "##       "
               , "##       "
               , "# #####  "
               , " ####### "   -- intention: a /five/ '5'
               , "        #"
               , "       ##"
               , "       ##"
               , "  ##### #"
               , " ####### "
               ]

          g6 = [ " ####### "
               , "# #####  "
               , "##       "
               , "##       "
               , "# #####  "
               , " ####### "   -- intention: a /six/ '6'
               , "#       #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "
               ]

          g7 = [ " ####### "
               , "  ##### #"
               , "       ##"
               , "       ##"
               , "        #"
               , "         "   -- intention: a /seven/ '7'
               , "        #"
               , "       ##"
               , "       ##"
               , "        #"
               , "         "
               ]

          g8 = [ " ####### "
               , "# ##### #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "   -- intention: an /eight/ '8'
               , "#       #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "
               ]

          g9 = [ " ####### "
               , "# ##### #"
               , "##     ##"
               , "##     ##"
               , "# ##### #"
               , " ####### "   -- intention: a /nine/ '9'
               , "        #"
               , "       ##"
               , "       ##"
               , "  ##### #"
               , " ####### "
               ]
{-
* TODO Next Steps

Highest prio topmost:
- handle frustration (splitting)
- monadic language (pairing?)
- helpers (corners)
- layering, small grid -> (bits of) Char
- energy, economy
- performance
-}
